/*  
 *          ****************************
 *          * Curle starting rewrite:  *
 *          * discord.io to discord.js *
 *          ****************************
 *          * Current date is 19/10/18 *
 *          ****************************
 *  
 * 			
 */

const UMessage = require('./UMessage');

/*
 * Represents a thing that can send and receive {@link UMessage}s.
 */
class Participant {
	/**
	 * @constructor
	 * @param {string} id Identifier to distinguish this participant from others.
	 * @param {string} type Platform of this participant, used to determine message formats.
	 * @param {Object} recipient Object that sends/receives messages. This is usually the object created by a node.js package for a fill-in-the-blank client.
	 * @param {function(UMessage)} distributor Function that handles receiving messages from abroad.
	 * @param {Object} options List of additional options, callbacks, etc. See code for details. (Good luck.)
	 */
	constructor(id, type, participant, distributor, options = {}) {
		let defaults = {
			join_callback: function(community){},
			refresh_channels: function(){},
			reload: function(){}
		};
		let o = Object.assign({}, defaults, options);
		/**
		 * String to identify this Participant.
		 */
		this.id = id;
		/**
		 * String to identify the platform of this Participant (e.g., 'discord', 'irc').
		 */
		this.type = type;
		/**
		 * Object that manages interfacing with the platform. This is usually the object created by a node.js package for a fill-in-the-blank client.
		 */
		this.participant = participant;
		/**
		 * When in the Course of logic events, it becomes necessary for one object to dissolve the lexical scope which has isolated it from another, and to assume among the powers of the globe, the separate and equal station to which the Laws of Javascript and Javascript's Documentation entitle it, a decent respect to the opinions of developers requires that it should declare the causes which impel it to the elevation.
		 */
		this.participant.parent = this;
		/**
		 * Method that takes a {@link UMessage} from a {@link Community} and sends it to wherever it needs to go.
		 */
		this.distribute = distributor;
		/**
		 * Array of {@link Community Communities} to which this Participant belongs.
		 */
		this.communities = [];
		/**
		 * Array of channels to which this Participant listens.
		 * Often, this list should be populated when the participant emits a 'ready' event.
		 */
		this.channels = [];
		/**
		 * Function to be called upon joining a community.
		 * @callback Participant~join_callback
		 * @param {Community} community
		 */
		this.join_callback = o.join_callback;
		/**
		 * Custom function used to refresh the list of channels to which this Participant should be listening.
		 */
		this.refresh_channels = o.refresh_channels;
		/**
		 * Custom function to reload this Participant. This function is intended to contain platform-specific operations, such as joining newly added channels.
		 */
		this.reload = o.reload;
		/**
		 * When this Participant is added to a {@link Community}, the Community calls this method, which appends the Community to this Participant's array and calls the join_callback method.
		 */
		this.join = function(community) {this.communities.push(community); this.join_callback(community);};
		/**
		 * Upon receiving a message from the wild (where the users are, not a {@link Community}), the Participant converts it to a {@link UMessage}. The UMessage is then passed into this `propagate` method, which sends it to each of this Participant's Communities. This method also optionally logs some information.
		 * @param {UMessage} message Message to be relayed to the rest of the world
		 */
		this.propagate = function(message) {
			if(process.env.NODE_ENV == 'debug') console.log('Propagating message from ' + message.source);
			this.communities.forEach(c => c.distribute(message));
		};
	}
}

module.exports = Participant;
