/*  
 *          ****************************
 *          * Curle starting rewrite:  *
 *          * discord.io to discord.js *
 *          ****************************
 *          * Current date is 20/10/18 *
 *          ****************************
 *  
 * 			
 */

/*
 * Represents the most basic message format, which can be interpreted by any Participant.
 * FIXME: As self-doubt and insecurity creep in, I wonder whether this should exist, or whether this functionality
 * 	should be farmed out to the clients themselves. Note that some message properties are already added in
 * 	the client modules. The advantage to keeping this here is that new platforms will have clear examples of
 * 	what's expected in a UMessage in one central location.
 */
class UMessage {
	/**
	 * Constructs the UMessage type.
	 * @constructor
	 * @param {Object} message - An object representing a platform-specific message.
	 * @param {string} type - Platform on which this message originated.
	 * @param {(string|Object)} src - Any string or object that can be used to identify where a message came from, so that it doesn't get sent back to that channel
	 * @return {UMessage} An object having this structure:
	 * 	{
	 * 		source: (string|Object),
	 * 		user:
	 * 			{
	 * 				displayName: string,
	 * 				realName: string
	 * 			},
	 * 		channel:
	 * 			{
	 * 				displayName: '#' + string
	 * 			},
	 * 		content: string
	 *	}
	 */
	constructor(message, type, src = null) {
		switch(type) {
		case 'discord':
			this.source = message.channel_name; // id of Discord channel, a number stored as a string
			this.user = {
				displayName: message.author.nick ? message.author.nick : message.author.username, //Users are handled by ID anyway - nick or not, this shouldn't matter.
				realName: message.author.username + '#' + message.author.id, // username + tag is the full Discord user identifier
				};
			this.channel = {
				displayName: message.channel.name, //added in discord_client.js
				};
			var content = message.content; //consider using the Discord.js cleanContent property to auto strip the emojis and mentions
			content = content.replace(/<:([^:]+):[0-9]*>/g,':$1:'); // NOTE: this filters emojis from <:crup:2380834...> to :crup:
			this.content = content;
			
			break;
		case 'irc':
			this.source = src; // id of IRC client, a string
			this.user = {
				displayName: message.nick,
				realName: message.user,
				};
			this.channel = {
				displayName: message.to, // NOTE: this is set in irc_client.js
				};
			this.content = message.text; // NOTE: this is also set in irc_client.js
			break;
		default: console.error('unknown message source type');
		}
		if(process.env.NODE_ENV == 'debug') console.log(this);
	}
}

module.exports = UMessage;

/*
 * CONVERSION COMPLETE
 * by Curle
 * on 20/10/18
 */
