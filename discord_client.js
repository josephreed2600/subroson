/*  
 *          ****************************
 *          * Curle starting rewrite:  *
 *          * discord.io to discord.js *
 *          ****************************
 *          * Current date is 19/10/18 *
 *          ****************************
 *  
 * 			Note: This is the big one. Expect many future commits to be from this file.
 */

// Pre-Definitions
//==========================================================================

// Configure logging for context 'discord_client'
var logger = require('./logger').get('discord_client');

logger.info('Importing requirements');			//wording pls
const Participant = require('./Participant');
const UMessage = require('./UMessage');
var Discord = require('discord.js');			//the all important change
var auth    = require('./auth');				//file left out for obvious reasons
logger.info('Import complete');

/*
 * TODO: 
 * move these into a JSON file, to allow
 * configurability in the bots/users ignored.
 */
var OtherRoson = ""; 							//planning ahead for having Subroson and Super-Roson in the same server;
var CurrentRoson = "";							//these allow the two to ignore each other.

var BotID = "";

//Initialization
//==========================================================================

/*
 * Difference with Discord.js requires that
 * .login() be called when constructing the
 * client.
 * 
 * This means it's easier to use a variable
 * and call it in the constructor
 * 
 * Additionally, it removes the need to use
 * discord_client.participant.on as a work
 * around to get at the actual Client
 * instance.
 * 
 * However, we need to call participant.on
 * at least once to build the participant.
 */
//logger.info("Logging into bot user.");
var BotClient = new Discord.Client();

/*
 * NOTE:
 * login() is a tricky one.
 * Once login is complete, it emits ready.
 * Once ready is finished, it calls back
 * to success or error.
 * 
 */
var token = BotClient.login(auth.token).then(prepBot).catch(logger.error("Log in error. Aborting"));
logger.info(token);


//User defined callbacks.

function prepBot() {
 
	logger.info("Logged in.");
 
 
 
 	/* 
 	* First, we need to identify who we are.
 	* We can either be Subroson or Super-Roson,
 	* so we need to have a way to differentiate
 	* between them.
 	* 
 	* This checks against the current ID, checks
    * if there's another Roson in the server, and
 	* sets the flag to ignore it.
	*/
 	
 	logger.info('Identifying subversion');
	logger.info("We are " + BotID);
 
 	if (BotID == auth.superroson) {
 		CurrentRoson = BotID;
 		OtherRoson = auth.subroson;
 		logger.info ("We are Super-Roson");
 	} else if (BotID == auth.subroson) {
 		CurrentRoson = BotID;
 		OtherRoson = auth.superroson;
 		logger.info ("We are Subroson");
 	} else {
 		logger.error ("We are not a Roson!")
 	}	
 
 
	
 	//TODO: identify why this switch statement results in every case triggering
 	/*switch (Discord.Client.user_id) { <- is it this? Client hasn't been initialized yet so it doesn't know its' own ID.
 	*	case auth.superroson:
 	*		OtherRoson = auth.subroson;
 	*		CurrentRoson = auth.superroson
 	*		logger.info("We are Super-Roson");
	*	case auth.subroson:
 	*		OtherRoson = auth.superroson;
 	 *		CurrentRoson = auth.subroson;
 	 *		logger.info("We are Subroson");
 	 *	default:
 	 *		logger.error("Version Not Identified");
 	}*/
 };
 




logger.info('Creating new discord_client');
/**
 * Instantiate {@link Participant} with a new Discord client and a method that handles sending messages from a {@link Community}.
 * 
 */
discord_client = new Participant('discord_client', 'discord', BotClient, function(message) {
	
	logger.info("Auth Token is " + auth.token + " from Participant");

	//Prepare channels and servers
	//==========================================================================
	
	/**
	 * List of all channel names available.
	 */

	var channels = [];
	var all = this.participant.channels;
	var servers = [];

	for(i in this.participant.servers) {
		servers.push(bot.participant.servers[i]);
	}

	/**
	 * Return the server object (!) for the server that contains the given channel id.
	 * @param {string} channel_id Snowflake of the channel
	 * @return {object} server containing channel_id
	 */
	var get_server = function(channel_id) {
		for(server in servers) {
			if(servers[server].channels.hasOwnProperty(channel_id)) return servers[server];
		}

		//TODO: By adding to the above function, we can fine-tune the servers;
		//      ie. we can stop it syncing messages across servers

		// if we get here something is very wrong--message has no source channel
	};
	/**
	 * Push all the target channel IDs into `channels`.
	 */
	//I can't see why this isn't enrolling all the channels.
	logger.info(all);
	for(c in all) {

		if('#' + all[c].name.toLowerCase() == message.channel.name.toLowerCase() && all[c].type == 0) { //if #channel = channel name & channel is text (as opposed to DM or Voice)
			channels.push(c); //push/pop
		} 
		/**
	 	* Different servers have different emojis, users, and channels, so we parse the UMessage on a per-channel basis to get the most relevant information for each destination.
	 	*/
		channels.forEach(channel => {
			if(channel != message.source) {	// don't send this back where it came from, please
				/**
				 * Parse and resolve emojis
				 */
				var emojis = get_server(channel).emojis;
				var content = message.content.replace(/:([a-zA-Z0-9_]{1,32}):/g,function(whole,name){
					for(emoji in emojis) {
						if(emojis[emoji].name == name)
							return '<' + whole + emoji + '>';  // this server has a match, so return resolved emoji 
					}
					return whole;  // no match, so return plaintext unchanged
				});
				/**
				 * TODO: Implement resolution of user and channel mentions
				 */
				/**
				 * Send the message to the channel
				 */
				this.participant.sendMessage({
					to: channel,
					message: '**' + message.user.displayName + ':** ' + content
				}); // message is sent
			} // end of if
		}); // end of channels.forEach
	}
}); // end of constructor call! yay!

/**
 * Add logger to participant. All the extra guts we want to store go in the Participant.participant. Is that bad? FIXME
 */
discord_client.participant.logger = logger;
logger.info('discord_client Initialized');

//Preparation
//==========================================================================

/**
 * The Discord client won't know what servers or channels it has access to
 * until it's set up and connected and generally ready.
 * Once it is ready, we can populate the list of channels that we can see.
 */

/*
 * TODO:
 * Rewrite this entire block.
 * Talk about spaghetti - plus, this isn't even being
 * populated!
 * 
 * I'll come up with a temporary fix, but this needs
 * a permanent solution.
 * 
 */
discord_client.participant.on('debug', function (message) {
	logger.debug(message);
});

discord_client.participant.on('ready', () => {
	BotID = discord_client.participant.user.id;


	logger.info('Enrolling channels');
	var channels = this.channels;
	for(c in channels) {
		var name = '#' + channels[c].name.toLowerCase();
		if(channels[c].type == 0 && !this.parent.channels.includes(name)) {
			this.parent.channels.push(name);
		}
		/*
		 * Some testing loops, to experiment with how Snowflakes
		 * and IDs are handled in channels.
		 * Also, names. Two functions, I think one of them has
		 * to be set manually, as it's from another class.
		 * 
		 * I can't tell.
		 */

		 logger.info("Channel " + channels[c] +
					 " has name " + channels[c].name +
					 " with channel id " + channels[c].channel_id + 
					 "/" + channels[c].id);

	}

	// We've changed our channel list. Rouse the troops.
	/*
	 * This boggles me. Assuming the parent of this function is
	 * discord_client.js, there's no mention of communities.
	 * 
	 * In fact, the word "communities" only appears here and
	 * in index.js in a comment. What is this referencing?
	 * 
	 * Community, UMessage and Participant all provide null.
	 * 
	 */
	//this.parent.communities.forEach(c => c.reload());
	logger.info('Ready');
});


//Listening
//==========================================================================

discord_client.participant.on('message', function (fmessage) {
	
	var ignore = false;
	// ignore our own messages, plus the other Roson.
	// This is what the switch statement was for.
	if(fmessage.author.id == CurrentRoson || fmessage.author.id == OtherRoson){
		
		logger.info("message is from a bot!");
		ignore = true;

	} 

	//testing, react to Gem's messages with a thinkeyes
	if(fmessage.author.id == auth.gem) {
		logger.info ("Message is from Gem! Reacting.");
		fmessage.react(fmessage.guild.emojis.get("501865047434330112"));
	}
	
	// UMessage expects a channel name starting with '#'
	for (i in this.channels) {
		logger.info ("name is " + i.name +
					 "\nid is " + i.id);
	}

	//Print some debugging info, to eliminate some of the confusion around
	//multiple IDs and names
	logger.info("message info: " + 
				"\nchannel name: " + fmessage.channel.name +
				"\nchannel displayname: " + fmessage.channel.displayName + //undefined
				"\nchannel_name: " + fmessage.channel_name +
				"\nchannel id: " + fmessage.channel_id + //undefined
				"\nchannel.id: " + fmessage.channel.id +
				"\nmessage content: " + fmessage.content +
				"\nauthor name: " + fmessage.author.name + //undefined
				"\nauthor displayname: " + fmessage.author.displayName + //undefined
				"\nauthor id: " + fmessage.author.id +
				"\nauthor user_id: " + fmessage.author.user_id + //undefined
				"\nauthor username: " + fmessage.author.username +
				"\nauthor discriminator: " + fmessage.author.discriminator);

	// UMessage expects a channel name starting with '#'
	fmessage.channel_name = '#' + fmessage.channel.name;
	
	// Some platforms don't support multi-line messages (e.g., IRC)
	// Split the message at newlines
	if (ignore == false) {
		var lines = fmessage.content.split('\n');
		lines.forEach(line => {
			fmessage.content = line; // haha suck on that, pass by reference
			var umessage = new UMessage(fmessage,'discord');
			this.parent.propagate(umessage);
		});
	}

});

// General format for sending messages
// discord_client.sendMessage({
//		to: channelID,
//		message: message
//	});

// Send it

module.exports = discord_client;


/*
 * CONVERSION COMPLETE
 * by Curle
 * on 21/10/18
 */
