// Expects the variable 'script_role' to be defined
exports.get = function(role) {
	var winston = require('winston');
	var log_format = winston.format.printf(
		info => {info.message = '[' + role + '] ' + info.message; return info;}
	);
	var logger = winston.createLogger({
		level: 'debug',
		format: winston.format.combine(
			log_format,
			winston.format.align()
			),
		transports: []
	});
	logger.add(new winston.transports.File({
		filename: role + '-error.log',
		level: 'error',
		format: winston.format.simple()
	}));
	logger.add(new winston.transports.File({
		filename: role + '-combined.log',
		format: winston.format.simple()
	}));
	if(process.env.NODE_ENV !== 'production') {
		logger.add(new winston.transports.Console({
			format: winston.format.simple()
		}));
	}
	return logger;
};
