// Configure logging for context 'discord_client'
var logger = require('./logger').get('discord_client');

logger.info('Importing stuff');
const Participant = require('./Participant');
const UMessage = require('./UMessage');
var Discord = require('discord.io');
var auth    = require('./auth');
logger.info('...done.');

logger.info('Creating new discord_client');
/**
 * Instantiate {@link Participant} with a new Discord client and a method that handles sending messages from a {@link Community}.
 */
discord_client = new Participant('discord_client', 'discord', new Discord.Client({token: auth.token, autorun: true}), function(message) {
	// This method takes a UMessage and sends it places.
	// UMessages are boring; we want to spice them up by resolving emojis and mentions.
	// 
	// Get information about where this message is going
	/**
	 * Channel IDs to which this message will be sent.
	 */
	var channels = [];
	/**
	 * List of all channel names available.
	 */
	var all = this.participant.channels;
	/**
	 * List of all servers available, for convenience.
	 */
	var servers = []
	for(i in this.participant.servers) {servers.push(this.participant.servers[i]);}
	/**
	 * Return the server object (!) for the server that contains the given channel id.
	 * @param {string} channel_id ID string-number-thing of a Discord channel
	 * @return {object} server containing channel_id
	 */
	var get_server = function(channel_id) {
		for(server in servers) {
			if(servers[server].channels.hasOwnProperty(channel_id)) return servers[server];
		}
		// if we get here something is very wrong--message has no source channel
	};
	/**
	 * Push all the target channel IDs into `channels`.
	 */
	for(c in all)
	if('#' + all[c].name.toLowerCase() == message.channel.displayName.toLowerCase() && all[c].type == 0) channels.push(c);
	/**
	 * Different servers have different emojis, users, and channels, so we parse the UMessage on a per-channel basis to get the most relevant information for each destination.
	 */
	channels.forEach(channel => {
		if(channel != message.source) {	// don't send this back where it came from, please
			/**
			 * Parse and resolve emojis
			 */
			var emojis = get_server(channel).emojis;
			var using_colons = false; // using colons in the message // FIXME: reeee do a thing, user-specific, something! wait what, hmmmm
			var emoji_with_colons = /:([a-zA-Z0-9_]{1,32}):/g;
			var emoji_without_colons = /([a-zA-Z0-9_]{1,32})/g;
			var either = /:?([a-zA-Z0-9_]{1,32}):?/g;
			var content = message.content.replace(either,function(whole,name){
				for(emoji in emojis) {
					if(emojis[emoji].name == name)
						return '<:' + name + ':' + emoji + '>';  // this server has a match, so return resolved emoji 
				}
				return whole;  // no match, so return plaintext unchanged
			});
			/**
			 * TODO: Implement resolution of user and channel mentions
			 */
			/**
			 * Send the message to the channel
			 */
			this.participant.sendMessage({
				to: channel,
				message: '**' + message.user.displayName + ':** ' + content
			}); // message is sent
		} // end of if
	}); // end of channels.forEach
}); // end of constructor call! yay!

/**
 * Add logger to participant. All the extra guts we want to store go in the Participant.participant. Is that bad? FIXME
 */
discord_client.participant.logger = logger;
logger.info('...done.');

/**
 * The Discord client won't know what servers or channels it has access to until it's set up and connected and generally ready.
 * Once it is ready, we can populate the list of channels that we can see.
 */
discord_client.participant.on('ready', function (evt) {
	logger.info('Enrolling channels');
	var channels = this.channels;
	for(c in channels) {
		var name = '#' + channels[c].name.toLowerCase();
		if(channels[c].type == 0 && !this.parent.channels.includes(name))
			this.parent.channels.push(name);
	}
	// We've changed our channel list. Rouse the troops.
	this.parent.communities.forEach(c => c.reload());
	logger.info('Ready');
});

/**
 * When the client receives a message, this function will be called. It must do two things:
 *     * Convert the Discord message into a {@link UMessage}
 *     * Pass the UMessage to the {@link Participant}.propagate method
 * Information about Discord message structure may or may not be found in literature for discord.io:
 *     * https://izy521.gitbooks.io/discord-io/content/Methods/Client.html
 *     * https://cadence.gq/misc/discord.io
 *     * https://www.gitbook.com/book/izy521/discord-io/details
 * More information can *certainly* be found by logging some messages and looking through them.
 */
discord_client.participant.on('message', function (user, user_id, channel_id, msg, e) {
	// ignore our own messages
	if(user_id == auth.id) return;
	
	// UMessage expects a channel name starting with '#'
	e.d.channel_name = '#' + this.channels[e.d.channel_id].name;
	
	// Some platforms don't support multi-line messages (e.g., IRC)
	// Split the message at newlines
	var lines = e.d.content.split('\n');
	lines.forEach(line => {
		e.d.content = line; // haha suck on that, pass by reference
		var message = new UMessage(e,'discord');
		this.parent.propagate(message);
	});
});

// General format for sending messages
// discord_client.sendMessage({
//		to: channelID,
//		message: message
//	});

// Send it

module.exports = discord_client;
