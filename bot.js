// Configure logging for context 'bot'
logger = require('./logger').get('bot');

logger.info('Importing stuff');
var Discord = require('discord.io');
var auth    = require('./auth');
logger.info('...done.');

logger.info('Creating new bot');
bot = new Discord.Client({
   token: auth.token,
   autorun: true
});
logger.info('...done.');

bot.on('ready', function (evt) {
	logger.info('Ready');
});

bot.on('message', function (user, user_id, channel_id, msg, e) {
	// ignore our own messages
	if(userID == auth.id) return;
});

// Send it
exports.bot = bot;
