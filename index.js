process.env.NODE_ENV = 'debug';
package = require('./package.json');

console.log('Starting ' + package.name + ' v' + package.version);

// Configure logging for context 'main'
logger = require('./logger').get('main');

// Construct Discord bot
logger.info('Initializing Discord bot');
bot = require('./bot').bot;
logger.info('...done.');
